<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>QObject</name>
    <message>
        <source>Thanks to all these cool people that helped contributing to cmake-issue-23611 all these years.</source>
        <translation>Vielen Dank an all die coolen Leute, die all die Jahre dazu beigetragen haben, cmake-issue-23611 zu unterstützen.</translation>
    </message>
</context>
</TS>
